module PywalColors
(
      getPywalColors
    , Color
) where

import System.IO
import System.Environment
import Control.DeepSeq
import Control.Exception

myMerge :: [a] -> [a] -> [a]
myMerge (a:as) (b:bs) = a : myMerge as bs
myMerge [] (b)        = b
myMerge a []          = a

colorNames = myMerge ["background", "foreground", "red"] $ map show [0..]

type Color = String

getHome = getEnv "HOME"

getColors :: IO [Color]
getColors = do
    home <- getHome
    let file = home ++ "/.cache/wal/colors"
    handle <- openFile file ReadMode
    contents <- hGetContents handle
    return $!! lines contents

defaultColors = [
          "#292D3E"
        , "#F07178"
        , "#C3E88D"
        , "#FFCB6B"
        , "#82AAFF"
        , "#C792EA"
        , "#89DDFF"
        , "#959DCB"
        , "#676E95"
        , "#F07178"
        , "#C3E88D"
        , "#FFCB6B"
        , "#82AAFF"
        , "#C792EA"
        , "#89DDFF"
        , "#FFFFFF"
                ]

getPywalColors = 
    do
    result <- try getColors ::  IO (Either SomeException [Color])
    let colors = case result of
                Left e -> defaultColors
                Right c -> c
    return (colors ++ repeat "#ffffff")
