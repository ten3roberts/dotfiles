--
-- xmonad example config file.
--
-- A template showing all available configuration hooks,
-- and how to override the defaults in your own xmonad.hs conf file.
--
-- Normally, you'd only override those defaults you care about.
--
import PywalColors

import Data.Monoid
import Data.Maybe
import System.Exit
import System.IO (hPutStrLn)
import XMonad hiding ( (|||) )  -- don't use the normal ||| operator
import XMonad.Actions.CycleWS
import XMonad.Actions.Promote
import XMonad.Actions.RotSlaves
import XMonad.Actions.WindowGo (runOrRaise)
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat)
import XMonad.Layout.Grid
import XMonad.Layout.LayoutCombinators   -- use ||| from LayoutCombinators instead
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.Named
import XMonad.Layout.Reflect
import XMonad.Layout.Spacing
import XMonad.Layout.Spiral
import XMonad.Layout.ThreeColumns
import XMonad.Layout.TwoPane
import XMonad.Util.EZConfig -- Emacs style keybindings
import XMonad.Util.Run
import XMonad.Util.SpawnOnce

import qualified XMonad.StackSet as W
import qualified Data.Map        as M

-- The preferred terminal program, which is used in a binding below and by
-- certain contrib modules.
--
myTerminal      = "st"

myFileManager   = "thunar"

-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Width of the window border in pixels.
--
myBorderWidth   = 2

-- modMask lets you specify which modkey you want to use. The default
-- is mod1Mask ("left alt").  You may also consider using mod3Mask
-- ("right alt"), which does not conflict with emacs keybindings. The
-- "windows key" is usually mod4Mask.
--
myModMask       = mod4Mask

-- The default number of workspaces (virtual screens) and their names.
-- By default we use numeric strings, but any string may be used as a
-- workspace name. The number of workspaces is determined by the length
-- of this list.
--
-- A tagging example:
--
-- > workspaces = ["web", "irc", "code" ] ++ map show [4..9]
--
windowCount :: X Int
windowCount = length . W.integrate' . W.stack . W.workspace . W.current . windowset <$> get

myMerge :: [a] -> [a] -> [a]
myMerge (a:as) (b:bs) = a : myMerge as bs
myMerge [] (b)        = b
myMerge a []          = a

myWorkspaces = take 7 . myMerge ["dev", "web", "media", "sys", "games", "misc"] $ map show [1..]
-- Border colors for unfocused and focused windows, respectively.
--

type Keybind = String
type ProgClass = String
type ProgCommand = String
type Prog = (Keybind, ProgClass, ProgCommand)

myProgKeys :: [Prog]
myProgKeys =
    [
      ("M-C-f", "firefox", "firefox")
    , ("M-e", myFileManager, myFileManager)
    , ("M-C-s", "spotify", "spotify")
    , ("M-C-d", "discord", "discord")
    ]

myKeys :: [(String, X ())]
myKeys =
    map (\(m, p, l) -> (m, spawn $ "switch_app " ++ p ++ " " ++ l)) myProgKeys

    ++

    [
      ("M-<Return>", spawn myTerminal)
    , ("M-p", spawn "$HOME/.config/rofi/launchers/launcher.sh") -- Application launcher
    , ("M-S-w", kill) -- Close window
    -- Layout switching
    , ("M-<Space>", sendMessage NextLayout) -- Next layout
    , ("M-t M-t", sendMessage $ JumpToLayout "tall")
    , ("M-t M-f", sendMessage $ JumpToLayout "spiral")
    , ("M-t M-p", sendMessage $ JumpToLayout "twoPane")
    , ("M-t M-g", sendMessage $ JumpToLayout "grid")
    , ("M-t M-m", sendMessage $ JumpToLayout "full")

    -- , ("M-S-<Space>", sendMessage $ XMonad.layoutHook conf) -- Reset layout

    , ("M-j", windows W.focusDown)              -- Focus down in stack
    , ("M-k", windows W.focusUp)                -- Focus up in stack
    , ("M-S-j", windows W.swapDown)             -- Swap with down
    , ("M-S-k", windows W.swapUp)               -- Swap with up
    , ("M-h", sendMessage Shrink)               -- Shrink master
    , ("M-l", sendMessage Expand)               -- Expand master

    , ("M-m", windows W.focusMaster)            -- Focus master
    , ("M-g", windows W.swapMaster)             -- Swap node with master
    , ("M-<Backspace>", promote)                -- Swap but keep order

    , ("M-C-j", rotSlavesDown)                  -- Rotate all windows down except master and keep focus in place
    , ("M-C-k", rotSlavesUp)                    -- Rotate all windows up except master and keep focus in place
    , ("M-S-r", rotAllDown)                 -- Rotate all the windows in the current stack
    , ("M-r", rotAllUp)                 -- Rotate all the windows in the current stack

    , ("M-S-t", withFocused $ windows . W.sink)   -- Tile floating window

    -- Window limits
    , ("M-a", sendMessage (IncMasterN 1))       -- Increase nodes in master
    , ("M-d", sendMessage (IncMasterN (-1)))    -- Decrease nodes in master
    , ("M-S-a", increaseLimit)       -- Increase nodes in master
    , ("M-S-d", decreaseLimit)    -- Decrease nodes in master
    -- Screen movement
    , ("M-.", nextScreen)  -- Switch focus to next monitor
    , ("M-,", prevScreen)  -- Switch focus to prev monitor
    , ("M-S-.", shiftToPrev)
    , ("M-S-,", shiftToNext)

    -- Workspace movement
    , ("M-z", prevWS)
    , ("M-x", nextWS)
    , ("M-S-z", shiftToPrev)
    , ("M-S-x", shiftToNext)

    , ("M-S-l", spawn "light-locker-command -l")
    , ("M-S-q", spawn "$HOME/.config/rofi/scripts/menu_powermenu.sh")
    , ("M-q", spawn "xmonad --recompile && xmonad --restart") -- Restart xmonad
    , ("<XF86AudioMute>",   spawn "amixer set Master toggle")  -- Bug prevents it from toggling correctly in 12.04.         , 
    , ("M-S-<Left>", spawn "xbacklight -5")
    , ("M-S-<Right>", spawn "xbacklight +5")
    , ("<XF86AudioLowerVolume>", spawn "amixer set Master 5%- unmute")         
    , ("<XF86AudioRaiseVolume>", spawn "amixer set Master 5%+ unmute")
    , ("<Print>", spawn "scrotd 0")
    ]

    -- -- Move to workspace 1-9
    -- ++
    -- zip $ (zipWith (++) (repeat "M-") (map show [1..9])) (repeat $ spawn "Thunar")

    -- ++
    -- -- Move window to workspace 1-9
    -- zip $ (zipWith (++) (repeat "M-") (map show [1..9]), (repeat $ spawn "Thunar"))

------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

mySpacing :: Integer -> Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing o i = spacingRaw True (Border i i i i) True (Border o o o o) True

mySpacing' = mySpacing 2 2
------------------------------------------------------------------------
-- Layouts:

-- You can specify and transform your layouts by modifying these values.
-- If you change layout bindings be sure to use 'mod-shift-space' after
-- restarting (with 'mod-q') to reset your layout state to the new
-- defaults, as xmonad preserves your old layout settings by default.
--
-- The available layouts.  Note that each layout is separated by |||,
-- which denotes layout choice.
--
myLimitWindows = id -- limitWindows 8
myLayout = avoidStruts $ named "tall" tiled ||| named "twoPane" twoPane ||| named "grid" grid ||| named "spiral" mySpiral ||| named "full" Full
  where
     -- default tiling algorithm partitions the screen into two panes
    tiled    = mySpacing'
               $ myLimitWindows
               $ Tall nmaster delta ratio

    twoPane  = mySpacing'
               $ TwoPane delta (1/2)

    grid     = mySpacing'
               $ myLimitWindows
               $ Grid

    mySpiral = mySpacing'
               $ myLimitWindows
               $ spiral (6/7)

     -- The default number of windows in the master pane
    nmaster  = 1

     -- Default proportion of screen occupied by master pane
    ratio    = 0.6

     -- Percent of screen to increment by when resizing panes
    delta    = 3/100



------------------------------------------------------------------------
-- Window rules:

-- Execute arbitrary actions and WindowSet manipulations when managing
-- a new window. You can use this to, for example, always float a
-- particular program, or have a client always appear on a particular
-- workspace.
--
-- To find the property name associated with a program, use
-- > xprop | grep WM_CLASS
-- and click on the client you're interested in.
--
-- To match on the WM_NAME, you can use 'title' in the same way that
-- 'className' and 'resource' are used below.
--
myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    -- , title =? "Mozilla Firefox"     --> doShift ( myWorkspaces !! 1 )
    , (className =? "firefox" <&&> resource =? "Dialog") --> doFloat  -- Float Firefox Dialog
    , className =? "Gimp"           --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore ]

------------------------------------------------------------------------
-- Event handling

-- * EwmhDesktops users should change this to ewmhDesktopsEventHook
--
-- Defines a custom handler function for X Events. The function should
-- return (All True) if the default handler is to be run afterwards. To
-- combine event hooks use mappend or mconcat from Data.Monoid.
--
myEventHook = mempty

------------------------------------------------------------------------
-- Status bars and logging

-- Perform an arbitrary action on each internal state change or X event.
-- See the 'XMonad.Hooks.DynamicLog' extension for examples.
--

------------------------------------------------------------------------
-- Startup hook

-- Perform an arbitrary action each time xmonad starts or is restarted
-- with mod-q.  Used by, e.g., XMonad.Layout.PerWorkspace to initialize
-- per-workspace layout choices.
--
-- By default, do nothing.
myStartupHook = do
    -- Screen locking
    spawnOnce "light-locker &"
    spawnOnce "picom &"

------------------------------------------------------------------------
-- Run xmonad with all the defaults we set up.
main :: IO ()
main =
    do

    -- Get colors from pywal
    colors <- getPywalColors
    let 
        ( color1 : color2 : color3 : color4 : color5 : color6 : color7 : color8 : color9 : color10 : color11 : color12: color13 : color14 : color15 : color16 : _) 
            = colors

    xmproc <- spawnPipe "xmobar -x 0"
    xmproc1 <- spawnPipe "xmobar -x 1"
    let myConfig = def {
        -- simple stuff
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = color9,
        focusedBorderColor = color2,

        -- key bindings
        -- keys               = myKeys,
        mouseBindings      = myMouseBindings,

        -- hooks, layouts
        layoutHook         = myLayout,
        manageHook = ( isFullscreen --> doFullFloat ) <+> myManageHook,
        handleEventHook    = fullscreenEventHook <+> myEventHook,
        startupHook = return () >> checkKeymap myConfig myKeys >> myStartupHook,

        logHook = dynamicLogWithPP xmobarPP
                        { ppOutput = \x -> hPutStrLn xmproc x >> hPutStrLn xmproc1 x
                        -- Current workspace in xmobar
                        , ppCurrent = xmobarColor (color1 ++ "," ++ color3) "" . wrap " [" "] " 

                        -- Visible but not current workspace
                        , ppVisible = xmobarColor color3 ""                

                        -- Hidden workspaces in xmobar
                        , ppHidden = xmobarColor color5 "" . wrap "*" "*"   

                        -- Hidden workspaces (no windows)
                        , ppHiddenNoWindows = xmobarColor color6 ""        

                        -- Title of active window in xmobar
                        , ppTitle = xmobarColor color8 "" . shorten 60     

                        -- Separators in xmobar
                        , ppSep =  " | "          

                        -- Urgent workspace
                        , ppUrgent = xmobarColor color12 "" . wrap "!" "!"  
                        , ppExtras  = [fmap (Just . xmobarColor color11 "" . show) windowCount]                           -- # of windows current workspace
                        , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
                        }
    } `additionalKeysP` myKeys
    xmonad . ewmh $ docks myConfig
