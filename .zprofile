export QT_QPA_PLATFORMTHEME='qt5ct'


# Default programs
export BROWSER="firefox"
# export TERMINAL="kitty"
export EDITOR="nvim"
export VISUAL="nvim"

export TERMINAL="st"

export PATH=$HOME/.nimble/bin:$HOME/.cabal/bin:$HOME/.scripts:$HOME/.cargo/bin:$HOME/.local/bin::/usr/local/go/bin:$PATH

# Cleanup home
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CONFIG_HOMR="$HOME/.config"
export ZDOTDIR="$HOME/.config/zsh"
export INPUTRC="$HOME/.config/inputrc"
export LESSHISTFILE="-"

# Vim color scheme
export VIM_DARK_COLORSCHEME="one"
export VIM_LIGHT_COLORSCHEME="one"
